# Description

This script will help you to manage your spam e-mails. It is executed periodically and learn your spam or non-spam e-mails. It works only with maildir/ folders and spamassassin.

# Usage

* Install script to /usr/local/bin

* Configure a cron job per user

Cronjob:

	cat << EOF >/etc/cron.d/maildir-sa-learner
	0 6-23 * * * user [ -x /usr/local/bin/maildir-sa-learner.sh ] && /usr/local/bin/maildir-sa-learner.sh
	EOF

* 1st run, it will create maildir folders sa-spam/ and sa-ham/

* Move your spams/hams to this folders

* Each further script execute will put mails to spamassasin learner binaries
